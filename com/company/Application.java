package com.company;

import com.sun.xml.internal.bind.v2.runtime.reflect.Lister;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Application {

    public void print(String[] args) {
        for (int i = 0; i < args.length; i++) {
            System.out.print(i + ": " + args[i] + " ");
        }
    }
    public Application(boolean convertEmptyCards, Path inputFilePath, Path outputFilePath,
                                     String fullSetName, String threeLetterCode) {
        StringBuilder result = new StringBuilder(
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                        "<cockatrice_carddatabase version=\"3\">\n" +
                        "<sets>\n" +
                        "        <set>\n" +
                        "            <name>" + threeLetterCode + "</name>\n" +
                        "            <longname>" + fullSetName + "</longname>\n" +
                        "        </set>\n" +
                        "</sets>\n" +
                        "<cards>"
        );
        String[] lineTokens = null;
        try (InputStream in = Files.newInputStream(inputFilePath);
             BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
            String line;
            int lineNumber = 0;

            while ((line = reader.readLine()) != null) {

                try {
                    if (lineNumber <= 1) { //ignoring top 2 rows
                        lineNumber++;
                        continue;
                    }
                    lineNumber++;

                    lineTokens = line.split("\t");

                    String colorIndicator = "";

                    if (lineTokens[0].startsWith("CW") | lineTokens[0].startsWith("UW") || lineTokens[0].startsWith("RW"))
                        colorIndicator += "W";
                    if (lineTokens[0].startsWith("CU") | lineTokens[0].startsWith("UU") || lineTokens[0].startsWith("RU"))
                        colorIndicator += "U";
                    if (lineTokens[0].startsWith("CB") | lineTokens[0].startsWith("UB") || lineTokens[0].startsWith("RB"))
                        colorIndicator += "B";
                    if (lineTokens[0].startsWith("CR") | lineTokens[0].startsWith("UR") || lineTokens[0].startsWith("RR"))
                        colorIndicator += "R";
                    if (lineTokens[0].startsWith("CG") | lineTokens[0].startsWith("UG") || lineTokens[0].startsWith("RG"))
                        colorIndicator += "G";

                    if (convertEmptyCards || (lineTokens.length>=10 ? !lineTokens[9].isEmpty() : false)) {
                        //noinspection StringConcatenationInsideStringBufferAppend
                        result.append(
                            "        <card>\n" +
                            "            <name>" + (lineTokens[0].isEmpty() ? "" : (lineTokens[0] + " ")) + (lineTokens[2].isEmpty() ? "" : lineTokens[2]) + "</name>\n" +
                            "            <set muId=\"0\">" + threeLetterCode + "</set>\n" +
                            (colorIndicator.indexOf('W') != -1 ? "            <color>W</color>\n" : "") +
                            (colorIndicator.indexOf('U') != -1 ? "            <color>U</color>\n" : "") +
                            (colorIndicator.indexOf('B') != -1 ? "            <color>B</color>\n" : "") +
                            (colorIndicator.indexOf('R') != -1 ? "            <color>R</color>\n" : "") +
                            (colorIndicator.indexOf('G') != -1 ? "            <color>G</color>\n" : "") +
                            "            <manacost>" + (lineTokens[3].isEmpty() ? "" : lineTokens[3]) + "</manacost>\n" +
                            "            <type>" + (lineTokens[4].isEmpty() ? "" : (lineTokens[4] + " ")) + (lineTokens[5].isEmpty() ? "" : lineTokens[5]) + (lineTokens.length >= 7 ? (lineTokens[6].isEmpty() ? "" : " � " + lineTokens[6]) : "") + "</type>\n" +
                            (lineTokens.length>=9 && !lineTokens[7].isEmpty() && !lineTokens[8].isEmpty() ? "            <pt>" + lineTokens[7] + "/" + lineTokens[8] + "</pt>" : "") +
                            "            <tablerow>0</tablerow>\n" +
                            "            <text>" + (lineTokens.length >= 10 ? (lineTokens[9].isEmpty() ? "" : lineTokens[9]) : "") + "</text>\n" +
                            "        </card>"
                            );
                    }

                } catch(ArrayIndexOutOfBoundsException exception) {
                    System.out.print(lineNumber + " ");
                    print(lineTokens);
                    System.out.println();
                }
            }
        } catch (IOException x) {
            x.printStackTrace();
        }

        result.append(
            "    </cards>\n" +
            "</cockatrice_carddatabase>"
        );

        byte[] outputData = result.toString().getBytes();

        try (OutputStream out = new BufferedOutputStream(Files.newOutputStream(outputFilePath))) {
            out.write(outputData, 0, outputData.length);
        } catch (IOException x) {
            x.printStackTrace();
        }
        exit();
    }
    public Application(boolean convertEmptyCards, Path inputFilePath, Path outputFilePath) {

        StringBuilder result = new StringBuilder(
                "mse version: 0.3.8\n" +
                        "game: magic\n" +
                        "stylesheet: new\n" +
                        "set info:\n" +
                        "\tsymbol: \n" +
                        "styling:\n" +
                        "\tmagic-new:\n" +
                        "\t\ttext box mana symbols: magic-mana-small.mse-symbol-font\n" +
                        "\toverlay: \n"
        );

        String[] lineTokens = null;
        try (InputStream in = Files.newInputStream(inputFilePath);
             BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
            String line;
            int lineNumber = 0;

            while ((line = reader.readLine()) != null) {

                try {
                    if (lineNumber <= 1) { //ignoring top 2 rows
                        lineNumber++;
                        continue;
                    }
                    lineNumber++;

                    lineTokens = line.split("\t");

                    String colorIndicator;

                    if (lineTokens[0].startsWith("CA") || lineTokens[0].startsWith("UA") || lineTokens[0].startsWith("RA"))
                        colorIndicator = "artifact";
                    else if (lineTokens[0].startsWith("CW") | lineTokens[0].startsWith("UW") || lineTokens[0].startsWith("RW"))
                        colorIndicator = "white";
                    else if (lineTokens[0].startsWith("CU") | lineTokens[0].startsWith("UU") || lineTokens[0].startsWith("RU"))
                        colorIndicator = "blue";
                    else if (lineTokens[0].startsWith("CB") | lineTokens[0].startsWith("UB") || lineTokens[0].startsWith("RB"))
                        colorIndicator = "black";
                    else if (lineTokens[0].startsWith("CR") | lineTokens[0].startsWith("UR") || lineTokens[0].startsWith("RR"))
                        colorIndicator = "red";
                    else if (lineTokens[0].startsWith("CG") | lineTokens[0].startsWith("UG") || lineTokens[0].startsWith("RG"))
                        colorIndicator = "green";
                    else if (lineTokens[0].startsWith("CL") | lineTokens[0].startsWith("UL") || lineTokens[0].startsWith("RL"))
                        colorIndicator = "land";
                    else
                        colorIndicator = "multicolor";

                    if(convertEmptyCards || (lineTokens.length>=10 ? !lineTokens[9].isEmpty() : false)) {
                        //noinspection StringConcatenationInsideStringBufferAppend
                        result.append(
                            "card:\n" +
                            "\thas styling: false\n" +
                            "\tname: " + (lineTokens[0].isEmpty() ? "" : (lineTokens[0] + " ")) + (lineTokens[2].isEmpty() ? "" : lineTokens[2]) + "\n" +
                            "\tcasting cost: " + (lineTokens[3].isEmpty() ? "" : lineTokens[3]) + "\n" +
                            "\tcard color: " + colorIndicator + "\n" +
                            "\tsuper type: " + (lineTokens[4].isEmpty() ? "" : (lineTokens[4] + " ")) + (lineTokens[5].isEmpty() ? "" : lineTokens[5]) + "\n" +
                            "\tsub type: " + (lineTokens.length >= 7 ? (lineTokens[6].isEmpty() ? "" : lineTokens[6]) : "") + "\n" +
                            "\trule text:" + "\n" + "\t" + "\t" + (lineTokens.length >= 10 ? (lineTokens[9].isEmpty() ? "" : lineTokens[9]) : "") + "\n" +
                            "\tflavor text: " + (lineTokens.length >= 11 ? (lineTokens[10].isEmpty() ? "" : lineTokens[10]) : "") + "\n" +
                            "\tpower: " + (lineTokens.length >= 8 ? (lineTokens[7].isEmpty() ? "" : lineTokens[7]) : "") + "\n" +
                            "\ttoughness: " + (lineTokens.length >= 9 ? (lineTokens[8].isEmpty() ? "" : lineTokens[8]) : "") + "\n" +
                            "\tillustrator: \n"
                        );
                    }
                } catch(ArrayIndexOutOfBoundsException exception) {
                    System.out.print(lineNumber + " ");
                    print(lineTokens);
                    System.out.println();
                }
            }
        } catch (IOException x) {
            x.printStackTrace();
        }

        result.append(
            "version control:\n" +
            "\ttype: none\n" +
            "apprentice code: "
        );

        byte[] outputData = result.toString().getBytes();

        try {
            OutputStream out = new BufferedOutputStream(new FileOutputStream(outputFilePath.toString() + "\\set"));
            out.write(outputData, 0, outputData.length);
            out.close();
        } catch (IOException x) {
            x.printStackTrace();
        }

        try {
            List<File> sources = new ArrayList<>();
            sources.add(new File(outputFilePath.toString() + "\\set"));
            Path source = Paths.get(outputFilePath.toString() + "\\card set.mse-set");
            Packager.packZip(new File(source.toString()), sources);
            Files.delete(Paths.get(outputFilePath.toString() + "\\set"));
        } catch (IOException e) {
            e.printStackTrace();
        }


        exit();
    }
    private void exit() {
        Parent root;
        try {
            root = FXMLLoader.load(getClass().getResource("closeDialog.fxml"));
            Stage stage = new Stage();
            stage.setTitle("");
            stage.setScene(new Scene(root, 165, 103));
            stage.setResizable(false);
            stage.getIcons().add(new Image(Main.class.getResourceAsStream("icon.png")));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
