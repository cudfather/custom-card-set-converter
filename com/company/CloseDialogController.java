package com.company;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ResourceBundle;

public class CloseDialogController implements Initializable{

    @FXML
    private Button closeButton;

    @Override
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
        closeButton.setOnAction(e -> Platform.exit());
    }

}
