package com.company;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class DialogController implements Initializable{
    @FXML
    private TextField fullSetNameTextField;
    @FXML
    private TextField threeLetterCodeTextField;
    @FXML
    private Button okButton;

    @Override
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
        okButton.setOnAction(e -> new Application(Controller.convertEmptyCards, Controller.inputFilePath,
                Controller.outputFilePath, fullSetNameTextField.getText(), threeLetterCodeTextField.getText()));
    }

//    public String getFullSetName() { return fullSetNameTextField.getText(); }
//
//    public String getThreeLetterCode() {return threeLetterCodeTextField.getText(); }
}
