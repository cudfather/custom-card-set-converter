package com.company;

/**
 * Created by M on 15/02/2016.
 */
public class NoFileOutputFormatSelectedException extends Exception {
    public NoFileOutputFormatSelectedException() {
        super("No file output format selected");
    }
}
