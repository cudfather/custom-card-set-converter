package com.company;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Dialog extends javafx.application.Application{

    @Override
    public void start(Stage secondaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("cockatriceDialog.fxml"));
        secondaryStage.setTitle("Choose the name for your set");
        secondaryStage.setScene(new Scene(root, 419, 144));
        secondaryStage.setResizable(false);
        secondaryStage.show();
    }

}
