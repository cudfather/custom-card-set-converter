package com.company;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ResourceBundle;

public class Controller implements Initializable{

    static boolean convertEmptyCards;
    static Path inputFilePath, outputFilePath;

    @FXML
    private TextField inputFileTextField;
    @FXML
    private TextField outputPathTextField;
    @FXML
    private Button inputFileBrowseButton;
    @FXML
    private Button outputPathBrowseButton;
    @FXML
    private CheckBox convertEmptyCardsCheckBox;
    @FXML
    private RadioButton cockatriceRadioButton;
    @FXML
    private RadioButton mseRadioButton;
    @FXML
    private Button convertButton;

    final ToggleGroup fileFormatGroup = new ToggleGroup();

    @Override
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
        inputFileBrowseButton.setOnAction(e -> inputFileTextField.setText(inputFileChooser()));
        outputPathBrowseButton.setOnAction(e -> outputPathTextField.setText(outputPathChooser()));
        convertButton.setOnAction(e -> convert());
        convertButton.setDisable(true);
        convertButton.defaultButtonProperty();
        cockatriceRadioButton.setOnAction(e -> convertButton.setDisable(false));
        cockatriceRadioButton.setToggleGroup(fileFormatGroup);
        mseRadioButton.setToggleGroup(fileFormatGroup);
        mseRadioButton.setOnAction(e -> convertButton.setDisable(false));
    }
    private String outputPathChooser() {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Choose output path");
        directoryChooser.setInitialDirectory(
                new File(System.getProperty("user.home"))
        );
        Stage fileChooserStage = new Stage();
        File selectedDirectory = directoryChooser.showDialog(fileChooserStage);
        if (selectedDirectory != null) {
            return selectedDirectory.getAbsolutePath();
        }
        return null;
    }
    private String inputFileChooser() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose input file");
        fileChooser.getExtensionFilters().addAll(
                new ExtensionFilter("Tab Separated Values", "*.tsv")
        );
        fileChooser.setInitialDirectory(
                new File(System.getProperty("user.home"))
        );
        Stage fileChooserStage = new Stage();
        File selectedFile = fileChooser.showOpenDialog(fileChooserStage);
        if (selectedFile != null) {
            return selectedFile.getAbsolutePath();
        }
        return null;
    }
    private void convert() {
        try {
            if (fileOutputFormat().equals(FileOutputFormat.COCKATRICE)) {
                convertEmptyCards = convertEmptyCardsCheckBox.isSelected();
                inputFilePath = Paths.get(inputFileTextField.getText());
                outputFilePath = Paths.get(outputPathTextField.getText() + "\\cards.xml");
                Parent root;
                Stage stage;
                try {
                    root = FXMLLoader.load(getClass().getResource("cockatriceDialog.fxml"));
                    stage = new Stage();
                    stage.setTitle("Choose the name for your set");
                    stage.setScene(new Scene(root, 419, 144));
                    stage.setResizable(false);
                    stage.getIcons().add(new Image(Main.class.getResourceAsStream("icon.png")));
                    stage.show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else
                new Application(
                    convertEmptyCardsCheckBox.isSelected(),
                    Paths.get(inputFileTextField.getText()),
                    Paths.get(outputPathTextField.getText())
                );
        } catch (NoFileOutputFormatSelectedException e) {
            e.printStackTrace();
        }
    }
    private FileOutputFormat fileOutputFormat() throws NoFileOutputFormatSelectedException {
        if (cockatriceRadioButton.isSelected()) return FileOutputFormat.COCKATRICE;
        if (mseRadioButton.isSelected()) return FileOutputFormat.MSE;
        throw new NoFileOutputFormatSelectedException();
    }
}